package var1;

public class Square extends Shape {
    private double length;

    public Square(double length) {
        this.length = length;
    }

    public double getArea(){
        return length*length;
    }
}

