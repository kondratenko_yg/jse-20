package var1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Circle circle1 = new Circle(2);
        Circle circle2 = new Circle(5);
        Rectangle rectangle1 = new Rectangle(5,3);
        Rectangle rectangle2 = new Rectangle(12,3);
        Rectangle rectangle3 = new Rectangle(5,1);
        Square square1 = new Square(5);
        Square square2 = new Square(6);
        Square square3 = new Square(7);

        List<Shape> shapes = new ArrayList<>();
        addToList(shapes,rectangle1,circle2,square3);
        System.out.println("Area of shapes = " + getArea(shapes));


        List<Circle> circles = new ArrayList<>();
        addToList(circles,circle2,circle1);
        System.out.println("Area of circles = " + getArea(circles));

        List<Rectangle> rectangles = new ArrayList<>();
        addToList(rectangles,rectangle2,rectangle3);
        System.out.println("Area of rectangles = " + getArea(rectangles));

        List<Square> squares = new ArrayList<>();
        addToList(squares,square2,square1);
        System.out.println("Area of squares = " + getArea(squares));

//        List<Integer> error = new ArrayList<>();
//        addToList(error,1);
    }

    @SafeVarargs
    public static <T extends Shape> void addToList(List<T> items, T...shapes){
        items.addAll(Arrays.asList(shapes));
    }

    public static double getArea(List<? extends Shape> items){
        double result = 0;
        for (Shape shape : items){
            result += shape.getArea();
        }
        return Math.round(result);
    }
}
